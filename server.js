// Port
var port = process.env.PORT || 9999,

// Packages
    express = require('express'),
    app = express(),
    bodyParser = require('body-parser'),
    apiRouter = express.Router(),
    mongoose = require('mongoose'),
    configDB = require('./config/database.js'),

// Schema
    Sample = require('./app/schema/bkSchema.Sample');

// Connect to MongoDB via Mongoose
mongoose.connect(configDB.url);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback() {
    console.log('Successful connection to mongodb at ' + configDB.url + '...');
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Routes
require('./app/router.API.js')(
    apiRouter,
    Sample
);

app.use('/API', apiRouter);

// Launch
app.listen(port);
console.log('Listening on port ' + port + '...');