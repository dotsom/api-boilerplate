module.exports = function (
    router,
    Sample
) {

    //GET request for all
    router.get('/sample/all', function (req, res) {
        Sample.find({}, function (err, samples) {
            if (err) {
                console.log(err);
            } else {
                res.status(200).send({
                    err: false,
                    message: 'All Samples',
                    type: 'Sample',
                    data: samples
                });
            }
        });
    });

    //POST request for new Sample
    router.post('/sample/new', function (req, res) {
        var newSample = new Sample(),
            errorCount = 0,
            errorMsgs = [];

        if (req.body.name) {
            newSample.name = req.body.name;
        } else {
            errorCount++;
            errorMsgs.push('You need to include a value "name"');
        }

        if (errorCount < 1) {
            newSample.save(function () {
                res.status(200).send({
                    err: false,
                    message: 'Sample saved',
                    type: 'Sample',
                    data: newSample
                });
            });
        } else {
            res.status(200).send({
                err: true,
                message: 'Error encountered',
                type: 'Sample',
                data: errorMsgs
            });
        }
    });
};