module.exports = function (
    router,
// Schema
    Sample
) {
    'use strict';

    router.get('/', function (req, res) {
        res.json({ message: 'Please consult the docs to use this API!' });
    });

    // Sample Controller
    require('./controllers/bkController.API.Sample')(
        router,
        Sample
    );
};


